package com.switfttech.banner.util;

import com.switfttech.banner.model.BannerDetail;
import com.switfttech.banner.model.Image;
import com.switfttech.banner.model.Nationality;
import com.switfttech.banner.payload.BannerDetailDto;
import com.switfttech.banner.payload.BannerDetailRequest;
import com.switfttech.banner.payload.ImageRequestDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingConstants;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface BannerDetailMapper {

    @Mapping(target = "nationalityId", source = "nationality")
    BannerDetailDto bannerDetailToBannerDetailDto(BannerDetail bannerDetail);

    BannerDetail bannerDetailDtoToBannerDetail(BannerDetailDto bannerDetailDto);

    BannerDetail bannerDetailRequestToBannerDetail(BannerDetailRequest bannerDetailRequest);

    //list of nationality lai list of nationalityId ma convert
    default List<UUID> mapNationalityToId(List<Nationality> nationality) {
        return nationality.stream()
                .map(Nationality::getId)
                .collect(Collectors.toList());
    }

    Nationality nationalityIdToNationality(UUID nationalityId);

    Image imageRequestDtoToImage(ImageRequestDto imageRequestDto);
}
