package com.switfttech.banner.enums;

import lombok.Getter;

@Getter
public enum BannerPlatform {
    ADMIN_WEB("Admin Web"),
    CUSTOMER_WEB("Customer Web"),
    PARTNER_WEB("Partner Web"),
    CUSTOMER_MOBILE("Customer Mobile"),
    PARTNER_MOBILE("Partner Mobile");

    private final String description;

    BannerPlatform(String description) {
        this.description = description;
    }

}
