package com.switfttech.banner.enums;

public enum KycStatus {
    VERIFIED,
    PENDING
}
