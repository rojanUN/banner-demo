package com.switfttech.banner.enums;

public enum Gender {
    MALE,
    FEMALE,
    All,
    OTHERS
}
