package com.switfttech.banner.enums;

import lombok.Getter;

@Getter
public enum BannerPlace {
    LOGIN_PAGE("Login Page"),
    SIGNUP_PAGE("Signup Page"),
    DASHBOARD("Dashboard"),
    AIRLINE_PAGE("Airline Page"),
    SUCCESS_PAGE("Success Page");

    private final String description;

    BannerPlace(String description) {
        this.description = description;
    }

}
