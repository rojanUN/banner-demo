package com.switfttech.banner.enums;

public enum BannerStatus {
    ACTIVE,
    PENDING,
    REJECTED,
    PAUSED
}
