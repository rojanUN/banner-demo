package com.switfttech.banner.repository;

import com.switfttech.banner.model.BannerDetail;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface BannerDetailRepository extends JpaRepository<BannerDetail, UUID> {
}
