package com.switfttech.banner.repository;

import com.switfttech.banner.model.Nationality;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface NationalityRepository extends JpaRepository<Nationality, UUID> {
}
