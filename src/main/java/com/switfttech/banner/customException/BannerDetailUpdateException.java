package com.switfttech.banner.customException;

public class BannerDetailUpdateException extends RuntimeException{
    public BannerDetailUpdateException(String message, Throwable cause){
        super(message, cause);
    }
}
