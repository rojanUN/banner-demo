package com.switfttech.banner.payload;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.switfttech.banner.enums.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
public class BannerDetailRequest {

    private String title;

    private String description;

    private LocalDate startDate;

    private LocalDate endDate;

    private Boolean isSpecificTime;

    private LocalTime startTime;

    private LocalTime endTime;

    private BannerPlatform bannerPlatform;

    private BannerPlace bannerPlace;

    private List<ImageRequestDto> imageRequestDtoList;

    private KycStatus kycStatus;

    private BannerStatus status;

    private Gender gender;

    private VisaType visaType;

    private List<UUID> nationalityId;

    private int floorAge;

    private int ceilingAge;

}
