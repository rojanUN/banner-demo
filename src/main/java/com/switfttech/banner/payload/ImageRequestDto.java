package com.switfttech.banner.payload;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

import java.util.UUID;

@Getter
@Setter
public class ImageRequestDto {
    private UUID id;
    private String fileName;
    private String fileType;

    @JsonIgnore
    private MultipartFile file;
}
