package com.switfttech.banner.payload;

import com.switfttech.banner.enums.BannerPlace;
import com.switfttech.banner.enums.BannerStatus;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;
import java.util.UUID;

@Data
@Getter
@Setter
public class BannerDetailDto {
    private UUID id;
    private String title;
    private Date startDate;
    private Date endDate;
    private BannerPlace bannerPlace;
    private Boolean isSpecificTime;
    private BannerStatus status;
    private List<UUID> nationalityId;
//    BannerType???
//    private ?? status;
}
