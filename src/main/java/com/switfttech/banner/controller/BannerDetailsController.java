package com.switfttech.banner.controller;

import com.switfttech.banner.enums.BannerStatus;
import com.switfttech.banner.payload.BannerDetailDto;
import com.switfttech.banner.payload.BannerDetailRequest;
import com.switfttech.banner.service.BannerDetailService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/banner")
public class BannerDetailsController {

    private final BannerDetailService bannerDetailService;

    @PostMapping("/create")
    public ResponseEntity<BannerDetailDto> createBanner(@ModelAttribute BannerDetailRequest bannerDetailRequest) throws IOException {
        BannerDetailDto createdBanner = bannerDetailService.createBanner(bannerDetailRequest);
        return ResponseEntity.status(HttpStatus.CREATED).body(createdBanner);
    }

    @GetMapping("/find-all")
    public ResponseEntity<List<BannerDetailDto>> findAllBanner() {
        List<BannerDetailDto> bannerList = bannerDetailService.findAllBanner();
        return ResponseEntity.status(HttpStatus.OK).body(bannerList);
    }

    @GetMapping("/{id}/find")
    public ResponseEntity<BannerDetailDto> findBannerById(@PathVariable("id") UUID bannerDetailId) {
        BannerDetailDto bannerDetail = bannerDetailService.findBannerById(bannerDetailId);
        return ResponseEntity.status(HttpStatus.OK).body(bannerDetail);
    }

    @PutMapping("/{id}/update")
    public ResponseEntity<BannerDetailDto> updateBannerDetail(@PathVariable("id") UUID bannerDetailId, @ModelAttribute BannerDetailRequest bannerDetailRequest) throws IOException {
        BannerDetailDto updatedBanner = bannerDetailService.updateBannerDetail(bannerDetailId, bannerDetailRequest);
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(updatedBanner);
    }

    @DeleteMapping("/{id}/delete")
    public ResponseEntity<String> deleteBanner(@PathVariable("id") UUID bannerDetailId) {
        return bannerDetailService.deleteBannerById(bannerDetailId);
    }

    @PutMapping("/{id}/status")
    public ResponseEntity<BannerDetailDto> changeBannerStatus(@PathVariable("id") UUID bannerDetailId, @RequestParam("status") BannerStatus status) {
        BannerDetailDto statusUpdatedBanner = bannerDetailService.changeBannerStatus(bannerDetailId, status);
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(statusUpdatedBanner);
    }

}
