package com.switfttech.banner.model;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToMany;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.UUID;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Nationality {
    @Id
    private UUID id;

    private String nationalityName;

    @ManyToMany(mappedBy = "nationality", cascade = CascadeType.PERSIST)
    private List<BannerDetail> bannerDetail;
}
