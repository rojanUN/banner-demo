package com.switfttech.banner.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.switfttech.banner.enums.*;
import jakarta.persistence.*;
import jakarta.validation.constraints.AssertTrue;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@Entity
public class BannerDetail {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

    @NotNull
    private String title;

    private String description;

    @NotNull
    private LocalDate startDate;

    @NotNull
    private LocalDate endDate;

    @Column(name = "is_specific_time")
    private Boolean isSpecificTime;

    private LocalTime startTime;

    private LocalTime endTime;

    @Enumerated(EnumType.STRING)
    private BannerPlatform bannerPlatform;

    @Enumerated(EnumType.STRING)
    private BannerPlace bannerPlace;

    @OneToMany(mappedBy = "bannerDetail", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Image> imageList = new ArrayList<>();

    @Enumerated(EnumType.STRING)
    private KycStatus kycStatus;

    @Enumerated(EnumType.STRING)
    private Gender gender;

    @Enumerated(EnumType.STRING)
    private VisaType visaType;

    @ManyToMany(fetch = FetchType.EAGER)
    private List<Nationality> nationality;

    @Enumerated(EnumType.STRING)
    private BannerStatus status;

    private int floorAge;

    private int ceilingAge;

    @PrePersist
    @PreUpdate
    private void setDefaultValues() {
        if (!isSpecificTime) {
            startTime = LocalTime.MIN;
            endTime = LocalTime.MAX;
        }
    }

    @AssertTrue(message = "startTime and endTime must be provided for the banner is setup for a specific time")
    public boolean isValidSpecificTime() {
        if (isSpecificTime) {
            return startTime != null && endTime != null;
        }
        return true;
    }

}
