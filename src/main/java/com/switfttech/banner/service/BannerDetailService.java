package com.switfttech.banner.service;

import com.switfttech.banner.enums.BannerStatus;
import com.switfttech.banner.model.BannerDetail;
import com.switfttech.banner.payload.BannerDetailDto;
import com.switfttech.banner.payload.BannerDetailRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

@Service
public interface BannerDetailService {

//    void createBanner(BannerDetail bannerDetail);
    List<BannerDetailDto> findAllBanner();
    BannerDetailDto findBannerById(UUID bannerDetailId);
    BannerDetailDto updateBannerDetail(UUID bannerDetailId, BannerDetailRequest bannerDetailRequest) throws IOException;
    ResponseEntity<String> deleteBannerById(UUID bannerDetailId);
    BannerDetailDto createBanner(BannerDetailRequest bannerDetailRequest) throws IOException;
    BannerDetailDto changeBannerStatus(UUID bannerDetailId, BannerStatus bannerStatus);
}
