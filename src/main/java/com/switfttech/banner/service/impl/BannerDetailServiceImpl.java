package com.switfttech.banner.service.impl;

import com.switfttech.banner.enums.BannerStatus;
import com.switfttech.banner.model.BannerDetail;
import com.switfttech.banner.model.Image;
import com.switfttech.banner.model.Nationality;
import com.switfttech.banner.payload.BannerDetailDto;
import com.switfttech.banner.payload.BannerDetailRequest;
import com.switfttech.banner.payload.ImageRequestDto;
import com.switfttech.banner.repository.BannerDetailRepository;
import com.switfttech.banner.repository.ImageRepository;
import com.switfttech.banner.repository.NationalityRepository;
import com.switfttech.banner.service.BannerDetailService;
import com.switfttech.banner.util.BannerDetailMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.*;

@Service
@RequiredArgsConstructor
public class BannerDetailServiceImpl implements BannerDetailService {
    private final BannerDetailRepository bannerDetailRepository;
    private final NationalityRepository nationalityRepository;
    private final ImageRepository imageRepository;
    private final BannerDetailMapper bannerDetailMapper;

    @Override
    public BannerDetailDto createBanner(BannerDetailRequest bannerDetailRequest) throws IOException {
        BannerDetail bannerDetail = bannerDetailMapper.bannerDetailRequestToBannerDetail(bannerDetailRequest);
        updateBannerDetailProperties(bannerDetail, bannerDetailRequest);
        bannerDetailRepository.save(bannerDetail);
        return bannerDetailMapper.bannerDetailToBannerDetailDto(bannerDetail);
    }

    @Override
    public List<BannerDetailDto> findAllBanner() {
        List<BannerDetail> bannerDetailList = bannerDetailRepository.findAll();
        List<BannerDetailDto> bannerDetailDtoList = new ArrayList<>();
        for (BannerDetail bannerDetail : bannerDetailList) {
            BannerDetailDto bannerDetailDto = bannerDetailMapper.bannerDetailToBannerDetailDto(bannerDetail);
            bannerDetailDtoList.add(bannerDetailDto);
        }
        return bannerDetailDtoList;
    }


    @Override
    public BannerDetailDto findBannerById(UUID bannerDetailId) {
        BannerDetail bannerDetail = bannerDetailRepository.findById(bannerDetailId)
                .orElseThrow(() -> new IllegalArgumentException("Not Found BannerDetail ID: " + bannerDetailId));
        return bannerDetailMapper.bannerDetailToBannerDetailDto(bannerDetail);
    }


    @Override
    public BannerDetailDto updateBannerDetail(UUID bannerDetailId, BannerDetailRequest bannerDetailRequest) {
        try {
            BannerDetail existingBannerDetail = bannerDetailRepository.findById(bannerDetailId)
                    .orElseThrow(() -> new IllegalArgumentException("Not Found BannerDetail ID: " + bannerDetailId));
            updateBannerDetailProperties(existingBannerDetail, bannerDetailRequest);
            BannerDetail updatedBannerDetail = bannerDetailRepository.save(existingBannerDetail);
            return bannerDetailMapper.bannerDetailToBannerDetailDto(updatedBannerDetail);
        } catch (IOException e) {
            throw new RuntimeException("Failed to update BannerDetail", e);
        }
    }


    @Override
    public ResponseEntity<String> deleteBannerById(UUID bannerDetailId) {
        Optional<BannerDetail> bannerDetailOptional = bannerDetailRepository.findById(bannerDetailId);
        if (bannerDetailOptional.isPresent()){
            bannerDetailRepository.deleteById(bannerDetailId);
            return ResponseEntity.status(HttpStatus.ACCEPTED).body("Banner with ID " + bannerDetailId + " deleted successfully.");
        }else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Banner with ID " + bannerDetailId + " does not exist");
        }
    }

    private void updateBannerDetailProperties(BannerDetail bannerDetail, BannerDetailRequest bannerDetailRequest) throws IOException {
        updateBasicProperties(bannerDetail, bannerDetailRequest);
        updateImages(bannerDetail, bannerDetailRequest);
        updateNationalities(bannerDetail, bannerDetailRequest);
    }

    private void updateBasicProperties(BannerDetail bannerDetail, BannerDetailRequest bannerDetailRequest) {
        bannerDetail.setTitle(bannerDetailRequest.getTitle());
        bannerDetail.setDescription(bannerDetailRequest.getDescription());
        bannerDetail.setStartDate(bannerDetailRequest.getStartDate());
        bannerDetail.setEndDate(bannerDetailRequest.getEndDate());
        bannerDetail.setIsSpecificTime(bannerDetailRequest.getIsSpecificTime());
        bannerDetail.setStartTime(bannerDetailRequest.getStartTime());
        bannerDetail.setEndTime(bannerDetailRequest.getEndTime());
        bannerDetail.setBannerPlatform(bannerDetailRequest.getBannerPlatform());
        bannerDetail.setBannerPlace(bannerDetailRequest.getBannerPlace());
        bannerDetail.setKycStatus(bannerDetailRequest.getKycStatus());
        bannerDetail.setGender(bannerDetailRequest.getGender());
        bannerDetail.setVisaType(bannerDetailRequest.getVisaType());
        bannerDetail.setFloorAge(bannerDetailRequest.getFloorAge());
        bannerDetail.setCeilingAge(bannerDetailRequest.getCeilingAge());
        bannerDetail.setStatus(BannerStatus.PENDING);
    }

    private void updateImages(BannerDetail bannerDetail, BannerDetailRequest bannerDetailRequest) throws IOException {
        List<Image> existingImages = bannerDetail.getImageList();
        if (existingImages == null) {
            existingImages = new ArrayList<>();
            bannerDetail.setImageList(existingImages);
        }
        //remove all images if null list in request
        if ( bannerDetailRequest.getImageRequestDtoList() == null || bannerDetailRequest.getImageRequestDtoList().isEmpty()) {
            imageRepository.deleteAll(existingImages);
            existingImages.clear();
            return;
        }
        List<Image> newImages = new ArrayList<>();
        List<String> newImageFileNames = new ArrayList<>();

        //update image file in newImage list if does not exist previously
        for (ImageRequestDto imageRequestDto : bannerDetailRequest.getImageRequestDtoList()) {
            MultipartFile file = imageRequestDto.getFile();
            if (!file.isEmpty()) {
                String fileName = file.getOriginalFilename();
                newImageFileNames.add(fileName);

                Image image = existingImages.stream()
                        .filter(img -> img.getFileName().equals(fileName))
                        .findFirst()
                        .orElse(new Image());

                image.setFileName(fileName);
                image.setFileType(file.getContentType());
                image.setData(file.getBytes());
                image.setBannerDetail(bannerDetail);

                newImages.add(image);
            }
        }

        List<Image> imagesToRemove = new ArrayList<>();
        //remove previous images missing in the new request
        for (Image existingImage : existingImages) {
            if (!newImageFileNames.contains(existingImage.getFileName())) {
                imagesToRemove.add(existingImage);
            }
        }
        existingImages.removeAll(imagesToRemove);
        imageRepository.deleteAll(imagesToRemove);

        //setting updated existingImages list as the final list
        for (Image newImage : newImages) {
            if (!existingImages.contains(newImage)) {
                existingImages.add(newImage);
            }
        }

        bannerDetail.setImageList(existingImages);
    }

    private void updateNationalities(BannerDetail bannerDetail, BannerDetailRequest bannerDetailRequest) {
        List<UUID> nationalityIdList = bannerDetailRequest.getNationalityId();
        List<Nationality> nationalityList = new ArrayList<>();
        for (UUID nationalityId : nationalityIdList) {
            Nationality nationality = nationalityRepository.findById(nationalityId)
                    .orElseThrow(() -> new IllegalArgumentException("Invalid Nationality ID: " + nationalityId));
            nationalityList.add(nationality);
        }
        bannerDetail.setNationality(nationalityList);
    }


    @Override
    public BannerDetailDto changeBannerStatus(UUID bannerDetailId, BannerStatus bannerStatus) {
        BannerDetail bannerDetail = bannerDetailRepository.findById(bannerDetailId)
                .orElseThrow(() -> new IllegalArgumentException("Not Found BannerDetail ID: " + bannerDetailId));

        bannerDetail.setStatus(bannerStatus);
        BannerDetail updatedBannerDetail = bannerDetailRepository.save(bannerDetail);

        return bannerDetailMapper.bannerDetailToBannerDetailDto(updatedBannerDetail);
    }

}
